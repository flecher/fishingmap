package com.flecher.fishingmap.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable

abstract class BaseFragment : DaggerFragment() {

    @get:LayoutRes
    abstract val layoutId: Int

    val disposable by lazy {
        CompositeDisposable()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(layoutId, container, false)
        view.post {
            postViewCreated(view)
        }
        return view
    }

    open fun postViewCreated(view: View) {}

    override fun onStop() {
        disposable.clear()
        super.onStop()
    }

}