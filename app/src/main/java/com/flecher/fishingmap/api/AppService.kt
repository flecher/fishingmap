package com.flecher.fishingmap.api

import com.flecher.fishingmap.api.model.LoginResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface AppService {

    @GET("login")
    fun login(
        @Query("username") username: String,
        @Query("password") password: String
    ): Observable<LoginResponse>

}