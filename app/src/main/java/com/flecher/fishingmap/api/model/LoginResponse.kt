package com.flecher.fishingmap.api.model

data class LoginResponse(
    val login: LoginData,
    val user: UserData,
    val scans: List<ScanData>
)

data class LoginData(
    val appId: String,
    val token: String,
    val userId: Int,
    val validated: Boolean,
    val validTill: Long
)

data class UserData(
    val userId: Int,
    val familyName: String,
    val name: String,
    val email: String,
    val locale: String,
    val subscribe: Boolean,
    val image: String?
)

data class ScanData(
    val id: Int,
    val name: String,
    val groupId: Int,
    val date: Long,
    val scanPoints: Int,
    val mode: Int
)