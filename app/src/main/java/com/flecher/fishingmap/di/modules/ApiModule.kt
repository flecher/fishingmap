package com.flecher.fishingmap.di.modules

import android.content.Context
import com.flecher.fishingmap.BuildConfig
import com.flecher.fishingmap.api.AppService
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
class ApiModule {

    @Provides
    fun provideOkHttpClient(context: Context, headers: Interceptor): OkHttpClient {
        val client = OkHttpClient.Builder()
        client.addInterceptor(headers)
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        client.addInterceptor(interceptor)
        return client.build()
    }

    @Provides
    fun provideMoshi(): Moshi {
        return Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    @Provides
    fun provideRetrofitBuilder(okHttpClient: OkHttpClient, moshi: Moshi): AppService {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.API_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(AppService::class.java)
    }

    @Provides
    fun getHeaderInterceptor(context: Context): Interceptor {
        return Interceptor { chain ->
            val requestBuilder = chain.request().newBuilder()
            requestBuilder.addHeader("Accept", "application/json")
            requestBuilder.addHeader("Content-Type", "application/json")
            requestBuilder.addHeader("X-App-Version", BuildConfig.VERSION_NAME)
            requestBuilder.addHeader("X-Device-Type", "Android")
            requestBuilder.addHeader("X-Accept-Version", "1.0")
            requestBuilder.addHeader("Accept-Language", "en")
//            PreferenceUtils.getToken(context)?.let {
//                requestBuilder.addHeader("Authorization", "Bearer $it")
//            }
            chain.proceed(requestBuilder.build())
        }
    }

}