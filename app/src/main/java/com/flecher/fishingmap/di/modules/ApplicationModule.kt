package com.flecher.fishingmap.di.modules

import android.content.Context
import com.flecher.fishingmap.FishingMapApp
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule {

    @Provides
    fun providesApplicationContext(app: FishingMapApp): Context = app.applicationContext
}