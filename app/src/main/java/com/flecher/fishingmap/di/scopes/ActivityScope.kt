package com.flecher.fishingmap.di.scopes

import javax.inject.Scope

@Scope
@Retention
annotation class ActivityScope