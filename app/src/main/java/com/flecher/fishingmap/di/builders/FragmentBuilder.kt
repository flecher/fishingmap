package com.flecher.fishingmap.di.builders

import com.flecher.fishingmap.ui.login.LoginFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector
    abstract fun loginFragment(): LoginFragment
}