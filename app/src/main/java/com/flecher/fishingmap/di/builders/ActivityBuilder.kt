package com.flecher.fishingmap.di.builders

import com.flecher.fishingmap.MainActivity
import com.flecher.fishingmap.di.scopes.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [FragmentBuilder::class])
    @ActivityScope
    abstract fun mainActivity(): MainActivity
}