package com.flecher.fishingmap.di

import com.flecher.fishingmap.FishingMapApp
import com.flecher.fishingmap.di.builders.ActivityBuilder
import com.flecher.fishingmap.di.modules.ApiModule
import com.flecher.fishingmap.di.modules.ApplicationModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@Component(
    modules = [
        ApplicationModule::class,
        ActivityBuilder::class,
        ApiModule::class,
        AndroidSupportInjectionModule::class
    ]
)
interface AppComponent : AndroidInjector<FishingMapApp> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<FishingMapApp>()
}