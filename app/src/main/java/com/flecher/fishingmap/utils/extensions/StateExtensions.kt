package com.flecher.fishingmap.utils.extensions

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.trafi.state.OnStateUpdate
import com.trafi.state.State
import com.trafi.state.StateListener
import com.trafi.state.StateMachine

fun <T : State<T, E>, E> StateMachine<T, E>.subscribeWithLifeCycle(
    lifeCycle: Lifecycle,
    onUpdate: OnStateUpdate<T>
) {
    val listener = object : StateListener<T, E> {
        override fun onStateUpdated(oldState: T, newState: T) = onUpdate(oldState, newState)
    }

    val observer = object : LifecycleObserver {
        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        fun start() = addListener(listener)
        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        fun stop() = removeListener(listener)
    }

    lifeCycle.addObserver(observer)

    onUpdate(null, state)
}