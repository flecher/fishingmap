package com.flecher.fishingmap.ui.login

import com.flecher.fishingmap.api.model.LoginResponse
import com.trafi.state.State

data class LoginState(
    val command: Command? = null,
    private val username: String? = null,
    private val password: String? = null
) : State<LoginState, LoginState.Event> {

    sealed class Event {
        data class TypedUsername(val text: String) : Event()
        data class TypedPassword(val text: String) : Event()
        object TappedLogin : Event()
        data class ReceivedLoginSuccess(val response: LoginResponse) : Event()
        data class ReceivedError(val apiError: Throwable) : Event()
    }

    sealed class Command {
        data class LogIn(val username: String, val password: String) : Command()
    }

    override fun reduce(event: Event): LoginState {
        return when (event) {
            is Event.ReceivedLoginSuccess -> copy()
            is Event.ReceivedError -> copy()
            Event.TappedLogin -> {
                when {
                    username.isNullOrBlank() -> {
                        copy()
                    }
                    password.isNullOrBlank() -> {
                        copy()
                    }
                    else -> copy(command = Command.LogIn(username, password))
                }
            }
            is Event.TypedUsername -> copy(username = event.text)
            is Event.TypedPassword -> copy(password = event.text)
        }
    }

    override fun clearCommand(): LoginState = copy(command = null)
}