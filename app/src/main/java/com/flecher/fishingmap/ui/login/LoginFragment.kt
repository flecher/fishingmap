package com.flecher.fishingmap.ui.login

import android.os.Bundle
import android.view.View
import com.flecher.fishingmap.base.BaseFragment
import com.flecher.fishingmap.R
import com.flecher.fishingmap.api.AppService
import com.flecher.fishingmap.utils.extensions.scheduleAsGetRequest
import com.flecher.fishingmap.utils.extensions.subscribeWithLifeCycle
import com.trafi.state.StateMachine
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class LoginFragment : BaseFragment() {

    override val layoutId: Int = R.layout.fragment_login

    @Inject
    lateinit var appService: AppService

    private val machine by lazy { StateMachine(LoginState()) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun postViewCreated(view: View) {
        machine.subscribeWithLifeCycle(viewLifecycleOwner.lifecycle) { boundState, newState ->
            if (boundState != null) {
                when (val command = newState.command) {
                    is LoginState.Command.LogIn -> {
                        login(command.username, command.password)
                    }
                }
            }
        }
    }

    private fun login(username: String, password: String) {
        appService
            .login(username, password)
            .scheduleAsGetRequest()
            .subscribe({
                machine.transition(LoginState.Event.ReceivedLoginSuccess(it))
            }, {
                machine.transition(LoginState.Event.ReceivedError(it))
            })
            .addTo(disposable)
    }
}